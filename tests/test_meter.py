from lifemetr.meter import LifeMetr

def test_checkin():
    lm = LifeMetr()
    lm.checkin('work')
    assert 'work' in lm.working

def test_checkout():
    lm = LifeMetr()
    lm.checkin('work')
    lm.checkout('work')

    assert 'work' not in lm.working
    assert lm.finished[-1].task == 'work'

    lm.checkout('work') # nothing happend

def test_restart():
    lm = LifeMetr()
    lm.checkin('work')
    lm.checkout('work')
    lm.restart(0)
    assert not lm.finished
    assert len(lm.working) == 1
    assert not lm.working['work'].end
