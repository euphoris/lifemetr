import sys

import iso8601

from lifemetr.meter import LifeMetr
from lifemetr.util import local_time


def print_task(task):
    print task.task
    print local_time(task.start)
    if task.end:
        print local_time(task.end)
    print task.duration
    print '-'*79


def select(lst, action):
    while True:
        for i in range(len(lst)):
            print '{}) {}'.format(i, lst[i])
        j = raw_input('? ')
        try:
            action(int(j))
            break
        except IndexError:
            print 'IndexError'
        except ValueError:
            if j == 'q':
                break
            print 'ValueError'


def valid_input(prompt, default):
    while True:
        try:
            return iso8601.parse_date(raw_input(prompt))
        except:
            return default


def main():
    lm = LifeMetr()
    with file('log.csv', 'rb') as f:
        lm.load(f)

    while True:
        cmd = raw_input(lm.prompt)

        if cmd == 'quit' or cmd == 'exit':
            with file('log.csv','wb') as f:
                lm.save(f)
            sys.exit()

        elif cmd == 'save':
            with file('log.csv','wb') as f:
                lm.save(f)

        elif cmd == 'new':
            task = raw_input('task: ')
            lm.tasks.add(task)

        elif cmd == 'checkin' or cmd == 'ci':
            tasks = list(lm.tasks)
            def ci_action(i):
                print local_time(lm.checkin(tasks[i]))
            select(tasks, ci_action)

        elif cmd == 'checkout' or cmd == 'co':
            tasks = lm.commits.keys()
            def co_action(j):
                print local_time(lm.checkout(tasks[int(j)]))
            select(tasks, co_action)

        elif cmd == 'log':
            for task in lm.finished:
                print_task(task)

            for task in lm.commits.values():
                print_task(task)

        elif cmd == 'delete':
            select(lm.finished, lm.delete)

        elif cmd == 'restart':
            select(lm.finished, lm.restart)

        elif cmd == 'edit':
            def edit_action(i):
                t = lm.finished[i]
                task = raw_input('task: ')
                if task:
                    t.task = task
                t.start = valid_input('check in: ', t.start)
                t.end = valid_input('check out: ', t.end)
            select(lm.finished, edit_action)

        elif cmd == 'stat':
            for task, duration in lm.stat().items():
                print duration, task

