import collections
from datetime import datetime

from babel.dates import format_datetime
from flask import Flask, render_template, redirect, request, url_for
import iso8601

from .meter import LifeMetr, DailyMetr
from .util import local_time


app = Flask(__name__)


class LifemetrContext():
    def __init__(self):
        self.lm = LifeMetr()

    def __enter__(self):
        with file('log.csv', 'rb') as f:
            self.lm.load(f)
        return self.lm

    def __exit__(self, type, value, traceback):
        with file('log.csv','wb') as f:
            self.lm.save(f)


def load(lm=None):
    if not lm:
        lm = LifeMetr()
    with file('log.csv', 'rb') as f:
        lm.load(f)
    return lm


@app.template_filter('local_time')
def local_time_filter(t):
    if t:
        return local_time(t)
    else:
        return ''

@app.template_filter('datetime')
def datetime_filter(t):
    if t:
        return format_datetime(local_time(t))
    else:
        return ''


@app.template_filter('duration')
def duration_filter(t):
    s = int(t)
    h = s / 3600
    m = s % 3600 / 60
    s = s % 60
    return '%d:%02d:%02d' % (h,m,s)


@app.route('/')
def today():
    return log(datetime.today().strftime('%Y-%m-%d'))

@app.route('/log/<day>')
def log(day):
    day = datetime.strptime(day, '%Y-%m-%d').date()
    lm = load(DailyMetr(day))


    return render_template('log.html',
        commits=lm.commits(),
        tasks=lm.tasks,
        stat=lm.stat(),
        working=lm.working)


@app.route('/daily')
def daily():
    lm = load()
    days = collections.defaultdict(collections.Counter)

    for _, commit in lm.commits():
        date = local_time(commit.start).date()
        days[date][commit.task] += commit.duration.seconds

    days = sorted(days.items(), key=lambda (d,_): d)

    return render_template('daily.html',
        days=days,
        tasks=lm.tasks)


@app.route('/checkin', methods=['POST'])
def checkin():
    task = request.form['task']
    with LifemetrContext() as lm:
        lm.checkin(task)
    return redirect(url_for('today'))


@app.route('/checkout/<task>', methods=['POST'])
def checkout(task):
    with LifemetrContext() as lm:
        lm.checkout(task)
    return redirect(url_for('today'))


@app.route('/delete/<int:commit_id>', methods=['POST'])
def delete(commit_id):
    with LifemetrContext() as lm:
        lm.delete(commit_id)
    return redirect(url_for('today'))


@app.route('/restart/<int:commit_id>', methods=['POST'])
def restart(commit_id):
    with LifemetrContext() as lm:
        lm.restart(commit_id)
    return redirect(url_for('today'))


@app.route('/edit/<int:commit_id>', methods=['GET'])
def edit_form(commit_id):
    lm = load()
    commit = lm.get_commit(commit_id)
    return render_template('edit.html', commit_id=commit_id, commit=commit)


def save_or_default(commit, attr, value):
    try:
        value = iso8601.parse_date(value)
    except:
        pass
    else:
        commit.__setattr__(attr, value)


@app.route('/edit/<int:commit_id>', methods=['POST'])
def edit_save(commit_id):
    with LifemetrContext() as lm:
        commit = lm.get_commit(commit_id)
        commit.task = request.form['task']
        save_or_default(commit, 'start', request.form['start'])
        save_or_default(commit, 'end', request.form['end'])
    return redirect(url_for('today'))


def main():
    app.debug = True
    app.run()
