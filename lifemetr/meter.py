import collections
import csv
from datetime import datetime, timedelta

from dateutil.tz import tzutc
import iso8601

from lifemetr.util import local_time


def now():
    return datetime.now(tzutc())

class Commit(object):
    def __init__(self, task):
        self.task = task
        self.start = now()
        self.end = None

    def checkout(self):
        self.end = now()

    @property
    def duration(self):
        if self.end:
            return self.end - self.start
        else:
            return  now() - self.start

    def __repr__(self):
        return ','.join([
            self.task,
            local_time(self.start).isoformat(),
            local_time(self.end).isoformat()])


class LifeMetr(object):
    def __init__(self):
        self.finished = []
        self.working = {}
        self.tasks = set()

    def commits(self):
        finished = zip(range(1, len(self.finished)+1), self.finished)
        working = [(None,c) for c in self.working.values()]
        return finished + working

    def get_commit(self, i):
        return self.finished[i-1]

    def checkin(self, task):
        if task in self.working:
            t = self.working[task]
        else:
            t = Commit(task)
            self.working[task] = t
        self.tasks.add(task)
        return t.start

    def checkout(self, task):
        try:
            t = self.working[task]
        except KeyError:
            return None
        else:
            t.checkout()
            self.finished.append(t)
            del self.working[task]
        return t.end

    @property
    def prompt(self):
        return ','.join(self.working.keys()) + '> '

    def load(self, f):
        for line in csv.reader(f):
            task = line[0]
            t = Commit(task)
            t.start = iso8601.parse_date(line[1])
            try:
                t.end = iso8601.parse_date(line[2])
                self.finished.append(t)
            except iso8601.ParseError:
                self.working[t.task] = t
            self.tasks.add(task)

    def save(self, f):
        writer = csv.writer(f)
        for _, task in self.commits():
            if task.end:
                end = str(task.end)
            else:
                end = ''
            writer.writerow([task.task, str(task.start), end])

    def restart(self, i):
        t = self.get_commit(i)
        t.end = None
        self.working[t.task] = t
        self.delete(i)

    def delete(self, i):
        del self.finished[i-1]

    def stat(self):
        category = collections.defaultdict(timedelta)
        for _, task in self.commits():
            category[task.task] += task.duration
        return category


class DailyMetr(LifeMetr):
    def __init__(self, date):
        self.date = date
        super(DailyMetr, self).__init__()

    def commits(self):
        _commits = super(DailyMetr, self).commits()
        return [(i, c) for (i, c) in _commits
            if local_time(c.start).date() == self.date or i is None]
