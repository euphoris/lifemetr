from dateutil.tz import tzlocal


def local_time(time):
    return time.astimezone(tzlocal())

